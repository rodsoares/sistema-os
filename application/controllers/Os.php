<?php

class Os extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->logado) {
            redirect('home/login');
        }

        $this->load->model('os_model', 'osM');
        $this->load->model('clientes_model', 'clientesM');
        $this->load->model('usuarios_model', 'usuariosM');
        $this->data['menuClientes'] = 'clientes';

        $this->formConfigs = [
            'os' => array(
                array(
                    'field'=>'dataInicial',
                    'label'=>'DataInicial',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'dataFinal',
                    'label'=>'DataFinal',
                    'rules'=>'trim|xss_clean'
                ),
                array(
                    'field'=>'garantia',
                    'label'=>'Garantia',
                    'rules'=>'trim|xss_clean'
                ),
                array(
                    'field'=>'descricaoProduto',
                    'label'=>'DescricaoProduto',
                    'rules'=>'trim|xss_clean'
                ),
                array(
                    'field'=>'defeito',
                    'label'=>'Defeito',
                    'rules'=>'trim|xss_clean'
                ),
                array(
                    'field'=>'status',
                    'label'=>'Status',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'observacoes',
                    'label'=>'Observacoes',
                    'rules'=>'trim|xss_clean'
                ),
                array(
                    'field'=>'clientes_id',
                    'label'=>'clientes',
                    'rules'=>'trim|xss_clean|required'
                ),
                array(
                    'field'=>'usuarios_id',
                    'label'=>'usuarios_id',
                    'rules'=>'trim|xss_clean|required'
                ),
                array(
                    'field'=>'laudoTecnico',
                    'label'=>'Laudo Tecnico',
                    'rules'=>'trim|xss_clean'
                )
            )
        ];
    }

    function index() {
        $this->gerenciar();
    }

    function gerenciar() {
        $dados['os'] = $this->osM->select();
        $this->load->view('os', $dados);
    }

    function adicionar() {
        $dados['clientes'] = $this->clientesM->select();
        $dados['usuarios'] = $this->usuariosM->select();
        $this->load->view('adicionarOs', $dados);
    }

    public function editar($id) {
        $dados['os'] = $this->osM->find($id);
        $this->load->view('editarOs', $dados);
    }

    public function visualizar() {
        
    }

    public function gravaos() {
        $dados = $this->input->post();
        $dados['situacao'] = 1;
        $dados['dataInicial'] = implode("-", array_reverse(explode("/", $dados['dataInicial'])));
        $os_id = $this->osM->insert($dados);

        /**
         * Criando pdf para ser enviado
         */
        $dados = [];
        $dados['os'] = $this->osM->find($os_id);
        $cliente_email = $dados['os']->clienteEmail;
        $pdf = $this->geraPdfOs( $dados );

        /**
         * Criando e enviando emails
         */
        $destinatario = $dados['os']->nomeCliente;
        $email = $cliente_email;
        $assunto = 'SystemOS - Resumo Ordem de Serviço';

        $mensagem = "<h3> InfoSys - Ordem de Serviço </h3>";
        $mensagem .= "<p> Estimado Sr(a).: <strong>". $dados['os']->nomeCliente ."</strong></p>";
        // nl2br: converte new line (enter) para comandos <br>
        $mensagem .= nl2br('Em anexo está um resumo do seu pedido de serviços em nosso sistema.');

        $senha = $this->input->post('senha');

        $this->load->library('email');

        $this->configura($senha);

        $this->email->from('infobytemovel@gmail.com', 'InfoSys');
        $this->email->to($email);

        $this->email->subject($assunto);

        // Adicionando arquivo anexo baseado na nomenclara e diretório que foi salvo
        $this->email->attach( APPPATH.'../pdf/os/os-'. $dados['os']->os .'.pdf' );

        //$filename = base_url('mkt/fusca.jpg');
        //$this->email->attach($filename);
        //$cid = $this->email->attachment_cid($filename);
        $this->email->message($mensagem);

        if ($this->email->send()) {
            // atribui para variáveis de sessão "flash"
            $this->session->set_flashdata('success_msg', 'E-mail Enviado com Sucesso');
            redirect('os');
        } else {
            $this->session->set_flashdata('error_msg', 'Erro no processo de envio do email.');
            redirect('os');
        }
    }

    public function gravaAlteracao() {
        $dados = $this->input->post();
        $this->osM->update($dados);
        redirect(base_url('os'));
    }

    public function deletar($id) {
        $this->osM->deletar($id);
        redirect(base_url('os'));
    }

    public function email($id) {
        $dados['os'] = $this->osM->find($id);
//        $this->load->view('inc/titulo');
        $this->load->view('os_email', $dados);
    }

    public function configura($senha) {
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'infobytemovel@gmail.com';
        $config['smtp_pass'] = '24802480';
        $config['charset'] = 'utf-8';
        // deve ser aspas duplas ""
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
    }

    public function envia() {
        $destinatario = $this->input->post('destinatario');
        $email = $this->input->post('email');
        $assunto = $this->input->post('assunto');

        $mensagem = "<h3> InfoSys - Ordem de Serviço </h3>";
        $mensagem .= "<p> Estimado Sr(a).: <strong> $destinatario </strong></p>";
        // nl2br: converte new line (enter) para comandos <br>
        $mensagem .= nl2br($this->input->post('mensagem'));

        $senha = $this->input->post('senha');

        $this->load->library('email');

        $this->configura($senha);

        $this->email->from('infobytemovel@gmail.com', 'InfoSys');
        $this->email->to($email);

        $this->email->subject($assunto);

        //$filename = base_url('mkt/fusca.jpg');
        //$this->email->attach($filename);
        //$cid = $this->email->attachment_cid($filename);
        $this->email->message($mensagem);

        if ($this->email->send()) {
            // atribui para variáveis de sessão "flash"
            $this->session->set_flashdata('mensa', 'E-mail Enviado com Sucesso');
            $this->session->set_flashdata('tipo', 1);
            redirect('os');
        } else {
            print_r($this->email->print_debugger());
        }
    }

    /**
     * Email com os recem criada
     */
    public function geraPdfOs( $dados ) {

        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $mpdf->writeHTML( $this->load->view('emails/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        $pdf = $mpdf->Output(APPPATH.'../pdf/os/os-'. $dados['os']->os .'.pdf', 'F');

        return $pdf;
        //$mpdf->Output();
    }

    private function enviaOsPorEmail() {}

}
