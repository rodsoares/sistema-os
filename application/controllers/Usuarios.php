<?php

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if (!$this->session->logado) {
            redirect('home/login');
        }

        $this->load->model('usuarios_model', 'usuariosM');
        $this->data['menuClientes'] = 'clientes';
    }

    /**
     * TODO: Remover gerenciar. Index, teoricamente faz mesma função que gerenciar()
     */
    function index() {
        $this->gerenciar();
    }

    function gerenciar() {
        $dados['usuarios'] = $this->usuariosM->select();
        $this->load->view('usuarios', $dados);
    }

    function adicionar() {
        $this->load->view('adicionarUsuario');
    }

    public function alterar($id) {
        // obtém os campos do cliente cujo id foi passado por parâmetro
        $dados['usuarios'] = $this->usuariosM->find($id);

        /**
         *  MD5 é uma via única. Ou seja, somente dá para criar e comparar o hash gerado.
         *  Sendo assim, não é possível fazer uma descriptografia do hash md5.
         */
        $dados['usuarios']->senha = '';

        $this->load->view('editarUsuario', $dados);
    }

    public function visualizar() {
        
    }

    public function gravaUsuario() {
        $dados = $this->input->post();
        $dados['ativo'] = 1;
        $dados['dataCadastro'] = implode("-", array_reverse(explode("/", $dados['dataCadastro'])));
        $this->usuariosM->insert($dados);
        redirect(base_url('usuarios'));
    }

    public function delete($id) {
        $this->usuariosM->delete($id);
        redirect(base_url('usuarios'));
    }

    public function gravaAlteracao() {
        $dados = $this->input->post();
        $this->usuariosM->update($dados);
        redirect(base_url('usuarios'));
    }

    public function update($id) {
        $this->db->where('id', $id['id']);
        return $this->db->update('os', $id);
    }

}
