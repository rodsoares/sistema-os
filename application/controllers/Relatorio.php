<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if (!$this->session->logado) {
            redirect('home/login');
        }

        $this->load->library('Pdf');
        $this->load->model('os_model', 'osM');
    }

    public function clientes() {
        // carrega a model clientes
        $this->load->model('clientes_model', 'clientes');

        // obtém os registros a serem exibidos no relatório
        $dados = $this->clientes->select();

        // define o título do relatório
        $this->pdf->setTitulo(utf8_decode('Relatório de Clientes - Ativos'));

        // define as colunas a serem exibidas
        $this->pdf->setColunas(array(utf8_decode('Código') => 15,
            'Nome' => 60,
            'Telefone' => 60,
            'E-mail' => 30));

        // uso do contador de páginas
        $this->pdf->AliasNbPages();
        // adiciona uma página
        $this->pdf->AddPage();

        // percorre as linhas obtidas
        foreach ($dados as $linha) {
            $this->pdf->Cell(15, 8, $linha->id . ' ', 0, 0, 'R');
            $this->pdf->Cell(60, 8, utf8_decode($linha->nomeCliente));
            $this->pdf->Cell(60, 8, $linha->telefone);
            $this->pdf->Cell(30, 8, utf8_decode($linha->email), 0, 1);
        }

        // gera o relatório
       return $this->pdf->Output();
    }

    public function os() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->select();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }

    public function osO() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->selectStatusO();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }

    public function osE() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->selectStatusE();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }

    public function osA() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->selectStatusA();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }

    public function osF() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->selectStatusF();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }

    public function osC() {
        // Instancia a classe mPDF
        $mpdf = new \Mpdf\Mpdf();

        // Define um Cabeçalho para o arquivo PDF
        $header = 'Relatórios de Ordem de serviço';
        $mpdf->SetHeader($header);

        // Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
        // página através da pseudo-variável PAGENO
        $mpdf->SetFooter('{PAGENO}');

        // Insere o conteúdo na nova página do arquivo PDF
        $dados['os'] = $this->osM->selectStatusC();
        $mpdf->writeHTML( $this->load->view('relatorios/os', $dados, TRUE));

        // Cria uma nova página no arquivo
        $mpdf->AddPage();

        // Gera o arquivo PDF
        return $mpdf->Output();
    }
}
