<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function verifica_sessao() {
        if (!$this->session->logado) {
            redirect('home/login');
        }

        $this->load->model('os_model', 'os');
    }

    public function index() {
        $this->verifica_sessao();
        $this->data['menuPainel'] = 'Painel';
//        $this->data['view'] = 'mapos/painel';

        $dados['os'] = $this->os->selectLastModified();
        $this->load->view('home', $dados);
    }

    public function login() {
        $this->load->view('login');
    }

    public function logar() {
        // habilita a exibição de um conjunto de informações da execução deste método
        // $this->output->enable_profiler(TRUE);
        // carrega a model com os métodos da tabela usuarios
        $this->load->model('usuarios_model', 'usuarios');

        // obtém os dados do form
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');

        $verifica = $this->usuarios->verificaUsuario($email, $senha);

        if (isset($verifica)) {
            $sessao['nome'] = $verifica->nome;
            $sessao['id'] = $verifica->id;
            $sessao['logado'] = true;
            $this->session->set_userdata($sessao);
            redirect('/');
        } else {
            $this->session->set_flashdata('error_msg', 'Credenciais inválidas. Tente novamente');
            // Uma chamada assim é meio confuso, não ? REFATORAR ISSO
            redirect('/home/login');
        }
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect();
    }

//    public function __construct() {
//        parent::__construct();
//        $this->load->model('home_model');
//    }
}
