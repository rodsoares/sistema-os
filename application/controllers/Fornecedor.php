<?php

class Fornecedor extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->logado) {
            redirect('home/login');
        }
        
        
        $this->load->model('fornecedor_model', 'fornecedorM');
        $this->data['menuClientes'] = 'clientes';

        // Validações
        $this->formConfigs = [
            'fornecedor' => [
                [
                    'field'=>'nome',
                    'label'=>'Nome',
                    'rules'=>'required|trim|is_unique[fornecedor.nome]|xss_clean'
                ],
                [
                    'field'=>'cnpj',
                    'label'=>'CNPJ',
                    'rules'=>'required|trim|is_unique[fornecedor.cnpj]|xss_clean'
                ],
                [
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|trim|valid_email|is_unique[fornecedor.email]|xss_clean'
                ],
                [
                    'field'=>'telefone',
                    'label'=>'Telefone',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'celular',
                    'label'=>'Celular',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'endereco',
                    'label'=>'Endereço',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'cidade',
                    'label'=>'Cidade',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'bairro',
                    'label'=>'Bairro',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'estado',
                    'label'=>'Estado',
                    'rules'=>'required|trim|xss_clean'
                ],
                [
                    'field'=>'cep',
                    'label'=>'CEP',
                    'rules'=>'required|trim|xss_clean'
                ],
            ]
        ];
    }

    function index() {
        $this->gerenciar();
    }

    function gerenciar() {
        $dados['fornecedor'] = $this->fornecedorM->select();
        $this->load->view('fornecedor', $dados);
    }

    function adicionar() {
        $this->load->view('adicionarFornecedor');
    }

    public function alterar($id) {
        // obtém os campos do cliente cujo id foi passado por parâmetro
        $dados['fornecedor'] = $this->fornecedorM->find($id);
        $this->load->view('editarFornecedor', $dados);
    }

    public function visualizar() {
        
    }

    public function gravaFornecedor() {
        /**
         * Validando campos
         */
        $this->form_validation->set_rules($this->formConfigs['fornecedor']);
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', $this->form_validation->error_array());
            redirect(base_url('fornecedor/adicionar'));
        } else {
            // verificando se CNPJ é valido
            if ( TRUE ) {
                $dados = $this->input->post();
                $this->fornecedorM->insert($dados);
                redirect(base_url('fornecedor'));
            } else {
                $this->session->set_flashdata('error_msg', ['CNPJ inválido.']);
                redirect(base_url('fornecedor/adicionar'));
            }

        }
    }

    public function delete($id) {
        $this->fornecedorM->delete($id);
        redirect(base_url('fornecedor'));
    }

    public function gravaAlteracao() {
        // recebe os dados do formulário
        $dados = $this->input->post();
        $this->fornecedorM->update($dados);
        // recarrega a view (index)
        redirect(base_url('fornecedor'));
    }

}
