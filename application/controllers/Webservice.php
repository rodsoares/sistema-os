<?php

class Webservice extends CI_Controller {


    function __construct() {
        parent::__construct();

        $this->load->model('clientes_model', 'clientesM');
        $this->load->model('usuarios_model', 'usuariosM');
        $this->load->model('os_model', 'osM');
    }


    function get_all_os() {
        // indica o tipo de retorno de método
        header('Content-Type: application/json');

        echo json_encode( $this->osM->select() );
    }

    function get_os($id = null) {

        // indica o tipo de retorno de método
        header('Content-Type: application/json');

        //verifica se o $id não foi passado
        if ($id == null) {
            $retorno = array("situacao" => "url incorreta",
                "id" => null,
                "cliente" => null,
                "defeito" => null,
                "status" => null,
                "laudoTecnico" => null,
                "valorTotal" => null);
        } else {

            //obtém o registro do id passado como parâmetro
            $osResult = $this->osM->find($id);

            if (isset($osResult)) {
                $retorno = array("situacao" => "encontrado",
                    "id" => $osResult->id,
                    "cliente" => $osResult->nomeCliente,
                    "tecnico" => $osResult->nome,
                    "descricao" => $osResult->descricaoProduto,
                    "defeito" => $osResult->defeito,
                    "status" => $osResult->status,
                    "laudotecnico" => $osResult->laudoTecnico,
                    "valorpecas" => $osResult->valorPecas,
                    "valorservico" => $osResult->valorServico,
                    "valortotal" => $osResult->valorTotal);
            } else {
                $retorno = array("situacao" => "inexistente",
                    "id" => null,
                    "cliente" => null,
                    "usuario" => null,
                    "tecnico" => null,
                    "status" => null,
                    "laudotecnico" => null,
                    "valortotal" => null);
            }
        }

        echo json_encode($retorno);
    }

}
