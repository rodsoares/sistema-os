<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->session->logado) {
            redirect('home/login');
        }

        $this->load->model('clientes_model');
        $this->data['menuClientes'] = 'clientes';

        $this->formConfigs = array(
            'clientes' => array(
                array(
                    'field'=>'nomeCliente',
                    'label'=>'Nome',
                    'rules'=>'required|trim|is_unique[clientes.nomeCliente]|xss_clean'
                ),
                array(
                    'field'=>'documento',
                    'label'=>'CPF/CNPJ',
                    'rules'=>'required|trim|is_unique[clientes.documento]|xss_clean'
                ),
                array(
                    'field'=>'telefone',
                    'label'=>'Telefone',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|trim|valid_email|is_unique[clientes.email]|xss_clean'
                ),
                array(
                    'field'=>'endereco',
                    'label'=>'Endereço',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'numero',
                    'label'=>'Número',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'bairro',
                    'label'=>'Bairro',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'cidade',
                    'label'=>'Cidade',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'estado',
                    'label'=>'Estado',
                    'rules'=>'required|trim|xss_clean'
                ),
                array(
                    'field'=>'cep',
                    'label'=>'CEP',
                    'rules'=>'required|trim|xss_clean'
                )
            )
        );
    }

    function index() {
        $config = array(
            "base_url" => base_url('clientes/p'),
            "per_page" => 3,
            "num_links" => 3,
            "uri_segment" => 3,
            "total_rows" => $this->clientes_model->CountAll(),
            "full_tag_open" => "<ul class='pagination'>",
            "full_tag_close" => "</ul>",
            "first_link" => FALSE,
            "last_link" => FALSE,
            "first_tag_open" => "<li>",
            "first_tag_close" => "</li>",
            "prev_link" => "Anterior",
            "prev_tag_open" => "<li class='prev'>",
            "prev_tag_close" => "</li>",
            "next_link" => "Próxima",
            "next_tag_open" => "<li class='next'>",
            "next_tag_close" => "</li>",
            "last_tag_open" => "<li>",
            "last_tag_close" => "</li>",
            "cur_tag_open" => "<li class='active'><a href='#'>",
            "cur_tag_close" => "</a></li>",
            "num_tag_open" => "<li>",
            "num_tag_close" => "</li>"
        );
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['clientes'] = $this->clientes_model->GetAll('nomeCliente', 'asc', $config['per_page'], $offset);
        $this->load->view('clientes', $data);
    }

    function gerenciar() {
        $dados['clientes'] = $this->clientes_model->select();
        $this->load->view('clientes', $dados);
    }

    public function visualizar() {
        
    }

    function adicionar() {
        $this->load->view('adicionarCliente');
    }

    /**
     * TODO: Por questões de seguranção e semantica, trocar método de acesso para POST
     */
    public function gravaCliente() {
        /**
         * Validando campos
         */
        $this->form_validation->set_rules($this->formConfigs['clientes']);

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error_msg', $this->form_validation->error_array());
            redirect(base_url('/clientes/adicionar'));
        } else {
            $dados = $this->input->post();

            // verificando se cpf é valido
            if ( $this->clientes_model->validaCPF( $dados['documento']) || $this->clientes_model->validaCNPJ( $dados['documento']) ) {
                $dados['ativo'] = 1;
                $this->clientes_model->insert($dados);
                redirect(base_url('clientes'));
            } else {
                $this->session->set_flashdata('error_msg', ['CPF/CNPJ inválido']);
                redirect(base_url('/clientes/adicionar'));
            }
        }

    }

    /**
     * TODO: Por questões de seguranção e semantica, trocar método de acesso para DELETE
     */
    public function delete($id) {
        $this->db->set('ativo', 0);
        $this->db->where('id', $id);
        $this->db->delete('clientes');

        $this->session->set_flashdata('success_msg', 'Cliente deletado com sucesso.');
        redirect(base_url('clientes'));
    }

    public function alterar($id) {
        // obtém os campos do cliente cujo id foi passado por parâmetro
        $dados['clientes'] = $this->clientes_model->find($id);
        $this->load->view('editarCliente', $dados);
    }

    /**
     * TODO: Por questões de seguranção e semantica, trocar método de acesso para PUT
     */
    public function gravaAlteracao() {
        $dados = $this->input->post();
        // Validar apenas CPF/CNPF
        if ( $this->clientes_model->validaCPF( $dados['documento']) || $this->clientes_model->validaCNPJ( $dados['documento']) ) {
            $this->clientes_model->update($dados);
            redirect(base_url('clientes'));
        } else {
            $this->session->set_flashdata('error_msg', ['CPF/CNPJ inválido']);
            redirect(base_url("/clientes/alterar/" . $dados['id']));
        }
    }

    /**
     * TODO: Isso não é para estar nesse controller!
     */
    public function update($id) {
        $this->db->where('id', $id['id']);
        return $this->db->update('os', $id);
    }
    
}
