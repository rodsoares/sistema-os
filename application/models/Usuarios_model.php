<?php

class Usuarios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function select() {
        $sql = "SELECT * FROM `usuarios` WHERE ativo = 1";
        $query = $this->db->query($sql);
        // retorna um registro
        return $query->result();
    }

    function insert($dados) {
        // Criptografando senha fornecida para padrão md5 antes de persistir dados no banco
        $dados['senha'] = md5( $dados['senha'] );

        return $this->db->insert('usuarios', $dados);
    }

    function delete($id) {
        $this->db->set('ativo', 0);
        $this->db->where('id', $id);
        $this->db->update('usuarios');
        return $this->db->affected_rows();
    }

    public function find($id) {
        $sql = "select * from usuarios where id= $id";
        $query = $this->db->query($sql);
        // retorna um registro
        return $query->row();
    }

    function count($table) {
        return $this->db->count_all($table);
    }

    public function update($usuario) {
        $this->db->where('id', $usuario['id']);

        // Criptografando senha fornecida para padrão md5 antes de persistir dados no banco
        $dados['senha'] = md5( $dados['senha'] );

        return $this->db->update('usuarios', $usuario);
    }

    public function verificaUsuario($email, $senha) {
        $sql = "select * from usuarios where email=? and senha=? and ativo=1";
        $query = $this->db->query($sql, array($email, md5($senha)));
        return $query->row();
    }

}
