<?php

class Os_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function select() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros a serem atualizados ( 7 )
     */
    public function selectLastModified() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                order by os.atualizado_em DESC
                limit 7";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros com status 'orçamento' a serem atualizados ( 7 )
     */
    public function selectStatusO() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                where os.status = 'O'
                order by os.atualizado_em DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros com status 'em andamento' a serem atualizados ( 7 )
     */
    public function selectStatusE() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                where os.status = 'E'
                order by os.atualizado_em DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros com status 'aberto' a serem atualizados ( 7 )
     */
    public function selectStatusA() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                where os.status = 'A'
                order by os.atualizado_em DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros com status 'finalizado' a serem atualizados ( 7 )
     */
    public function selectStatusF() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                where os.status = 'F'
                order by os.atualizado_em DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * Retorna ultimos registros com status 'cancelado' a serem atualizados ( 7 )
     */
    public function selectStatusC() {
        //$sql = "select *, os.id as os from os inner join clientes on os.clientes_id=clientes.id where situacao = 1";
        $sql = "select os.valorTotal, os.id as os, os.dataInicial, os.atualizado_em, os.status, os.defeito, os.clientes_id, clientes.nomeCliente as cliente
                from os 
                inner join clientes on os.clientes_id=clientes.id
                where os.status = 'C'
                order by os.atualizado_em DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function insert($dados) {
        $this->db->insert('os', $dados);
        return $this->db->insert_id();
    }

    function deletar($id) {
        $this->db->set('situacao', 0);
        $this->db->where('id', $id);
        $this->db->delete('os');
        return $this->db->affected_rows();
    }

    public function find($id) {
        $sql = "select *, os.id as os, clientes.email as clienteEmail from os inner join clientes on os.clientes_id=clientes.id inner join usuarios on os.usuarios_id=usuarios.id where os.id= $id";
        $query = $this->db->query($sql);
// retorna um registro
        return $query->row();
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('id', $id);
        // altera os dados
        return $this->db->delete('');
    }

    function count($table) {
        return $this->db->count_all($table);
    }

    public function update($os) {
        $this->db->where('id', $os['id']);
        // setando data de atualização
        $os['atualizado_em'] = (new DateTime('now'))->format('Y-m-d H:i:s');
        return $this->db->update('os', $os);
    }

    public function findEmail($email) {
        $sql = "select * from clientes where email = '$email'";
        $query = $this->db->query($sql);
        // row retorna o registro obtido
        $row = $query->row();
        return isset($row); // retorna true or false        
    }
    
}
