<?php

class Clientes_model extends CI_Model {
    
    var $table = "clientes";

    function __construct() {
        parent::__construct();
    }

    public function select() {
        $sql = "SELECT * FROM `clientes` WHERE ativo = 1";
        $query = $this->db->query($sql);
        // retorna um registro
        return $query->result();
    }

    function insert($dados) {
        return $this->db->insert('clientes', $dados);
    }

    public function find($id) {
        $sql = "select * from clientes where id= $id";
        $query = $this->db->query($sql);
        // retorna um registro
        return $query->row();
    }

    function delete($id) {
        $this->db->set('ativo', 0);
        $this->db->where('id', $id);
        $this->db->update('clientes');
        return $this->db->affected_rows();
    }

    function count($table) {
        return $this->db->count_all($table);
    }

    public function update($cliente) {
        $this->db->where('id', $cliente['id']);
        return $this->db->update('clientes', $cliente);
    }

    function GetAll($sort = 'id', $order = 'asc', $limit = null, $offset = null) {
        $this->db->order_by($sort, $order);
        if ($limit){
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function CountAll() {
        return $this->db->count_all($this->table);
    }

    /****************************************************************************************
     * funções 'helpers'
     ****************************************************************************************/
    function validaCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;
        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    function validaCPF($cpf) {

        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }
        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }


}
