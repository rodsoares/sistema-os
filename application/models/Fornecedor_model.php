<?php

class Fornecedor_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function select() {
        $sql = "select * from fornecedor";
        $query = $this->db->query($sql);
// retorna um registro
        return $query->result();
    }

    function insert($dados) {
        return $this->db->insert('fornecedor', $dados);
    }

    public function find($id) {
        $sql = "select * from fornecedor where id= $id";
        $query = $this->db->query($sql);
// retorna um registro
        return $query->row();
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('id', $id);
        // altera os dados
        return $this->db->delete('fornecedor');
    }

    function count($table) {
        return $this->db->count_all($table);
    }

    public function update($cliente) {
        $this->db->where('id', $cliente['id']);
        return $this->db->update('fornecedor', $cliente);
    }

    /****************************************************************************************
     * funções 'helpers'
     ****************************************************************************************/
    function validaCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;
        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

}
