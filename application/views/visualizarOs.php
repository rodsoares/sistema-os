<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>
<div class="container" style="margin-left: 3%">
    <div class="row-fluid" style="margin-top:0; max-width: 90%">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-tags"></i>
                    </span>
                    <h5>Cadastro de OS</h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                        <ul class="nav nav-tabs">
                            <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="span12" id="divCadastrarOs">
                                    <form method="post" action="<?= base_url('os/gravaos') ?>"
                                          enctype="multipart/form-data">

                                        <div class="span12" style="padding: 1%">
                                            <div class="span6">
                                                <label for="clientes_id">Cliente<span class="required">*</span></label>
                                                <select name="clientes_id" id="nomeCliente" class="form-control" required>
                                                    <option value=""> Selecione... </option>
                                                    <?php foreach ($clientes as $cliente) { ?>
                                                        <option value="<?= $cliente->id ?>"> <?= $cliente->nomeCliente ?> </option>                            
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="span6">
                                                <label for="usuario">Técnico/Usuário<span class="required">*</span></label>
                                                <select name="usuarios_id" id="nome" class="form-control" required>
                                                    <option value=""> Selecione... </option>
                                                    <?php foreach ($usuarios as $usuario) { ?>
                                                        <option value="<?= $usuario->id ?>"> <?= $usuario->nome ?> </option>                            
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="span12" style="padding: 1%; margin-left: 0.1em">
                                            <div class="span4">
                                                <label for="status">Status<span class="required">*</span></label>
                                                <select class="form-control" name="status" id="status" value="">
                                                    <option value="Orçamento">Orçamento</option>
                                                    <option value="Aberto">Aberto</option>
                                                    <option value="Em Andamento">Em Andamento</option>
                                                    <option value="Finalizado">Finalizado</option>
                                                    <option value="Cancelado">Cancelado</option>
                                                </select>
                                            </div>
                                            <div class="span4">
                                                <label for="dataInicial">Data entrada:<span class="required">*</span></label>
                                                <input id="dataInicial" class="form-control" type="text" name="dataInicial" value="<?php echo date('d/m/Y'); ?>"  />
                                            </div>

                                            <div class="span4">
                                                <label for="tipo">Tipo: <span class="required">*</span></label>
                                                <input id="tipo" class="form-control" type="text" name="tipo">
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span6">
                                                <label for="descricaoProduto">Descrição Equipamento:</label>
                                                <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"></textarea>
                                            </div>
                                            <div class="span6">
                                                <label for="defeito">Defeito</label>
                                                <textarea class="span12" name="defeito" id="defeito" cols="30" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span6">
                                                <label for="observacoes">Observações</label>
                                                <textarea class="span12" name="observacoes" id="observacoes" cols="30" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span6 offset3" style="text-align: center">
                                                <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                                <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php include 'inc/rodape.php' ?>
