<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">
    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading">
            Fornecedores > listagem geral
        </div>
        <div class="panel-body">
            <div style='margin-top: 20px'>
                <a href="<?php echo base_url(); ?>fornecedor/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Fornecedor</a>
            </div>

            <div class="widget-title table-bordered" style='margin-top: 30px'>
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Fornecedores</h5>
            </div>



            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Razão Social</th>
                        <th>CNPJ</th>
                        <th>Telefone</th>
                        <th colspan="2">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if ( $fornecedor ): ?>
                            <?php foreach ($fornecedor as $for) { ?>
                                <tr class="text-center">
                                    <td> <?= $for->id ?> </td>
                                    <td> <?= $for->nome ?> </td>
                                    <td> <?= $for->cnpj ?> </td>
                                    <td> <?= $for->telefone ?> </td>
                                    <td>
                                        <a href="<?= base_url() . 'fornecedor/delete/' . $for->id; ?>"
                                           onclick="return confirm('Confirma Exclusão do Fornecedor \'<?= $for->nome ?>\'?')">
                                            <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                                    </td>

                                    <td>
                                        <a href="<?= base_url() . 'fornecedor/alterar/' . $for->id ?>"
                                           onclick="return confirm('Gostaria de alterar os dados do Fornecedor \'<?= $for->nome ?>\'?')">
                                            <span class="glyphicon glyphicon-pencil" title="alterar"></span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php else: ?>
                            <tr class="bg-warning text-center">
                                <td colspan="6">
                                    <b>
                                        SEM RESULTADOS
                                    </b>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>






