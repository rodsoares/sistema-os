<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">

    <div class="widget-title table-bordered" style='margin-top: 30px'>
        <span class="icon">
            <i class="icon-user"></i>
        </span>
        <h5>Administração</h5>
    </div>
    </br>    </br>    </br>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="widget-title table-bordered" style='margin-top: 30px'>
                                <span class="icon">
                                    <i class="icon-user"></i>
                                </span>
                <h5>Ordem Serviço - Ultimas Alterações</h5>
            </div>


            <div class="widget-content nopadding">

                <table class="table table-bordered">
                    <thead>
                    <tr>

                        <th>OS</th>
                        <th>Cliente</th>
                        <th>Data </th>
                        <th>Defeito</th>
                        <th>Status</th>
                        <th colspan="3">Ações</th>
                    </thead>
                    <tbody>
                    <?php if ( $os ):?>
                        <?php foreach ($os as $o) { ?>
                            <tr class="text-center">
                                <td> <?= $o->os ?> </td>
                                <td> <?= $o->cliente?> </td>
                                <td> <?= date_format(date_create($o->atualizado_em), "d/m/Y H:i:s") ?> </td>
                                <td> <?= $o->defeito ?> </td>
                                <td>
                                    <?php if ( $o->status === 'O'): ?>
                                        Orçamento
                                    <?php elseif ( $o->status === 'E'): ?>
                                        Em andamento
                                    <?php elseif ( $o->status === 'A'): ?>
                                        Aberto
                                    <?php elseif ( $o->status === 'F'): ?>
                                        Finalizado
                                    <?php elseif ( $o->status === 'C'): ?>
                                        Cancelado
                                    <?php endif?>
                                </td>

                                <td>
                                    <a href="<?= base_url() . 'os/deletar/' . $o->os; ?>"
                                       onclick="return confirm('Confirma Exclusão da OS \'<?= $o->os ?>\'?')">
                                        <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                                </td>

                                <td>
                                    <a href="<?= base_url() . 'os/editar/' . $o->os ?>"
                                       onclick="return confirm('Gostaria de alterar os dados da OS \'<?= $o->os ?>\'?')">
                                        <span class="glyphicon glyphicon-pencil" title="Editar"></span></a>
                                </td>

                                <td>
                                    <a href="<?= base_url() . 'os/email/' . $o->os ?>">
                                        <span class="glyphicon glyphicon-envelope" title="Enviar E-mail"></span></a> &nbsp;&nbsp;
                                </td>
                            </tr>
                        <?php } ?>
                    <?php else: ?>
                        <tr class="text-center bg-warning">
                            <td colspan="8">
                                <b>SEM RESULTADOS</b>
                            </td>
                        </tr>
                    <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <br />
    <center>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            $.getJSON('/webservice/get_all_os', function ( response ) {
                var em_orcamento = new Array();
                var em_andamento = new Array();
                var aberto = new Array();
                var finalizada = new Array();
                var cancelada = new Array();

                for( var cont=0; cont < response.length; cont++) {
                    if (response[cont].status == 'O')
                        em_orcamento.push(response[cont]);

                    if (response[cont].status == 'E')
                        em_andamento.push(response[cont]);

                    if ( response[cont].status == 'A')
                        aberto.push( response[cont] );

                    if ( response[cont].status == 'F')
                        finalizada.push( response[cont] );

                    if ( response[cont].status == 'C')
                        cancelada.push( response[cont] );

                    console.log(em_orcamento);
                }

                // Load the Visualization API and the corechart package.
                google.charts.load('current', {'packages': ['corechart']});

                // Set a callback to run when the Google Visualization API is loaded.
                google.charts.setOnLoadCallback(drawChart);

                // Callback that creates and populates a data table,
                // instantiates the pie chart, passes in the data and
                // draws it.
                function drawChart() {

                    // Create the data table.
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Topping');
                    data.addColumn('number', 'Slices');
                    data.addRows([
                        ['OS Abertas', aberto.length],
                        ['OS Fechadas', finalizada.length],
                        ['OS Canceladas', cancelada.length],
                        ['OS Em andamento', em_andamento.length],
                        ['OS Orcamento', em_orcamento.length]

                    ]);

                    // Set chart options
                    var options = {'title': 'Ordens de Serviço',
                        'width': 500,
                        'height': 400};

                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            })
        </script>

        <!--Div that will hold the pie chart-->
        <div id="chart_div"></div>
    </center>
</div>

<?php include 'inc/rodape.php' ?>















