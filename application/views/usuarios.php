<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">

    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading"> Usuários > listagem geral</div>
        <div class="panel-body">
            <div style='margin-top: 20px'>
                <a href="<?php echo base_url(); ?>usuarios/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Usuario</a>
            </div>

            <div class="widget-title table-bordered" style='margin-top: 30px'>
        <span class="icon">
            <i class="icon-user"></i>
        </span>
                <h5>Usuários cadastrados no sistema</h5>
            </div>


            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Data Cadastro</th>
                        <th>CPF</th>
                        <th colspan="2">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($usuarios as $usu) { ?>
                        <tr class="text-center">
                            <td> <?= $usu->id ?> </td>
                            <td> <?= $usu->nome ?> </td>
                            <td> <?= date_format(date_create($usu->dataCadastro), "d/m/Y") ?> </td>
                            <td> <?= $usu->cpf ?> </td>


                            <td>
                                <a href="<?= base_url() . 'usuarios/delete/' . $usu->id; ?>"
                                   onclick="return confirm('Confirma Exclusão do Usuario \'<?= $usu->nome ?>\'?')">
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                            </td>

                            <td>
                                <a href="<?= base_url() . 'usuarios/alterar/' . $usu->id ?>"
                                   onclick="return confirm('Gostaria de alterar os dados do Usuário \'<?= $usu->nome ?>\'?')">
                                    <span class="glyphicon glyphicon-pencil" title="alterar"></span></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'inc/rodape.php' ?>






