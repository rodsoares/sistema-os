<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">

    <?php if( $this->session->flashdata('success_msg') ): ?>
        <div class="alert alert-success text-center" role="alert" style="margin-top: 10px;">
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>
    <?php elseif( $this->session->flashdata('error_msg') ): ?>
        <div class="alert alert-danger text-center" role="alert" style="margin-top: 10px;">
            <?php echo $this->session->flashdata('error_msg'); ?>
        </div>
    <?php endif ?>

    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading">
            Ordem de Serviços > listagem geral
        </div>
        <div class="panel-body">
            <div style='margin-top: 20px'>
                <a href="os/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar OS</a>
            </div>

            <div class="widget-title table-bordered" style='margin-top: 30px'>
        <span class="icon">
            <i class="icon-user"></i>
        </span>
                <h5>Ordem Serviço</h5>
            </div>


            <div class="widget-content nopadding">

                <table class="table table-bordered">
                    <thead>
                    <tr>

                        <th>OS</th>
                        <th>Cliente</th>
                        <th>Data </th>
                        <th>Defeito</th>
                        <th>Status</th>
                        <th colspan="3">Ações</th>
                    </thead>
                    <tbody>
                        <?php if ( $os ):?>
                            <?php foreach ($os as $o) { ?>
                                <tr class="text-center">
                                    <td> <?= $o->os ?> </td>
                                    <td> <?= $o->cliente?> </td>
                                    <td> <?= date_format(date_create($o->dataInicial), "d/m/Y") ?> </td>
                                    <td> <?= $o->defeito ?> </td>
                                    <td>
                                        <?php if ( $o->status === 'O'): ?>
                                            Orçamento
                                        <?php elseif ( $o->status === 'E'): ?>
                                            Em andamento
                                        <?php elseif ( $o->status === 'A'): ?>
                                            Aberto
                                        <?php elseif ( $o->status === 'F'): ?>
                                            Finalizado
                                        <?php elseif ( $o->status === 'C'): ?>
                                            Cancelado
                                        <?php endif?>
                                    </td>

                                    <td>
                                        <a href="<?= base_url() . 'os/deletar/' . $o->os; ?>"
                                           onclick="return confirm('Confirma Exclusão da OS \'<?= $o->os ?>\'?')">
                                            <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                                    </td>

                                    <td>
                                        <a href="<?= base_url() . 'os/editar/' . $o->os ?>"
                                           onclick="return confirm('Gostaria de alterar os dados da OS \'<?= $o->os ?>\'?')">
                                            <span class="glyphicon glyphicon-pencil" title="Editar"></span></a>
                                    </td>

                                    <td>
                                        <a href="<?= base_url() . 'os/email/' . $o->os ?>">
                                            <span class="glyphicon glyphicon-envelope" title="Enviar E-mail"></span></a> &nbsp;&nbsp;
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php else: ?>
                            <tr class="text-center bg-warning">
                                <td colspan="8">
                                    <b>SEM RESULTADOS</b>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<?php include 'inc/rodape.php' ?>
