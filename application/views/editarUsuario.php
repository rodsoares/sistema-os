<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">
    <div class="widget-title table-bordered" style='margin-top: 30px'>
        <span class="icon">
            <i class="icon-user"></i>
        </span>
        <h5>Editar Usuário</h5>
    </div>
    <br/>


    <div style="max-width: auto; margin:0">
        <div class="col-lg-12 col-md-12">
            <div class="row">
                <form method="post" action="<?= base_url('usuarios/gravaAlteracao') ?>"
                      enctype="multipart/form-data">

                    <input type="hidden" name="id" value="<?= $usuarios->id ?>">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nome"> Nome: </label>
                            <input type="text" id="nome" name="nome"
                                   value="<?= $usuarios->nome ?>"
                                   class="form-control" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="email"> E-mail </label>
                            <input type="text" id="email" name="email"
                                   value="<?= $usuarios->email ?>"
                                   class="form-control" required>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="senha"> Senha: </label>
                            <input type="text" id="documento" name="senha"
                                   value="<?= $usuarios->senha ?>"
                                   class="form-control" required>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="cpf"> CPF: </label>
                            <input type="text" id="documento" name="cpf"
                                   value="<?= $usuarios->cpf ?>"
                                   class="form-control" required>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="dataCadastro">Data Cadastro:</label>
                            <input class="form-control" id="dataCadastro" type="text" name="dataCadastro" 
                                   value="<?= $usuarios->dataCadastro ?>" disabled="">
                        </div>
                    </div>
                    <br/>
                    <div class="form-actions">
                        <div class="span12">
                            <div class="span6 offset3">
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Alterar Dados</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>