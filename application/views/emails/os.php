<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SystemOS - Email de Resumo de OS</title>
    </head>
    <body>
        <div style="text-align: center">
            <h1>SystemOS</h1>
            <h4>Resumo pedido de ordem de serviço</h4>
        </div>

        <hr />

        <div>
            <h5>Data:</h5>
            <p><?php echo (\DateTime::createFromFormat('Y-m-d', $os->dataInicial))->format('d/m/Y') ?></p>
        </div>
        <div>
            <h5>Status</h5>
            <p>ANÁLISE DE ORÇAMENTO</p>
        </div>

        <div>
            <h5>Cliente</h5>
            <p><?php echo $os->nomeCliente ?></p>
        </div>

        <div>
            <h5>Técnico responsável</h5>
            <p><?php echo $os->nome ?></p>
        </div>

        <div>
            <h5>Descrição</h5>
            <p><?php echo $os->descricaoProduto ?></p>
        </div>

        <div>
            <h5>Defeito</h5>
            <p><?php echo $os->defeito ?></p>
        </div>

        <div>
            <h5>Observações</h5>
            <p><?php echo $os->observacoes ?></p>
        </div>
    </body>
</html>