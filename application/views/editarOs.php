<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>

<div class="container" style="margin-left: 3%">
    <div class="row-fluid" style="margin-top:0; max-width: 90%">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-tags"></i>
                    </span>
                    <h5>Alteração de OS</h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="span12" id="divProdutosServicos" style=" margin-left: 0">
                        <ul class="nav nav-tabs">
                            <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                        </ul>

                        <div class="tab-content" style="padding: 1%">
                            <form method="post" action="<?= base_url('os/gravaAlteracao') ?>"
                                  enctype="multipart/form-data">
                                <div class="tab-pane active" id="tab1">
                                    <div class="span12" id="divCadastrarOs">


                                        <div class="span12" style="padding: 1%">
                                            <div class="span8">
                                                <input type="hidden" id="id" name="id" value="<?= $os->os ?>" >
                                                <label for="clientes_id">Cliente:</label>
                                                <input id="clientes_id" class="form-control" type="text" name="clientes_id" value="<?= $os->nomeCliente ?>" disabled >

                                            </div>

                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0.1em">
                                            <div class="span4">
                                                <label for="status">Status<span class="required">*</span></label>
                                                <select class="form-control" name="status" id="status">                                       
                                                    <option value="O" <?= $os->status == 'O' ? ' selected="selected"' : ''; ?>>Orçamento</option>
                                                    <option value="E" <?= $os->status == 'E' ? ' selected="selected"' : ''; ?>>Em andamento</option>
                                                    <option value="A" <?= $os->status == 'A' ? ' selected="selected"' : ''; ?>>Aberto</option>
                                                    <option value="F" <?= $os->status == 'F' ? ' selected="selected"' : ''; ?>>Finalizado</option>
                                                    <option value="C" <?= $os->status == 'C' ? ' selected="selected"' : ''; ?>>Cancelado</option>
                                                </select>
                                            </div>
                                            <div class="span4">
                                                <label for="dataInicial">Data entrada:<span class="required">*</span></label>
                                                <input class="form-control" type="text" name="dataInicial" value="<?php echo date('d/m/Y'); ?>" disabled />
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span6">
                                                <label for="descricaoProduto">Descrição Equipamento:</label>
                                                <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"><?php echo $os->descricaoProduto; ?></textarea>
                                            </div>
                                            <div class="span6">
                                                <label for="defeito">Defeito:</label>
                                                <textarea class="span12" name="defeito" id="defeito" cols="30" rows="5"><?php echo $os->defeito; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="span6">
                                                <label for="laudoTecnico">Laudo:</label>
                                                <textarea class="span12" name="laudoTecnico" id="laudoTecnico" cols="30" rows="5"><?php echo $os->laudoTecnico; ?></textarea>
                                            </div>
                                        </div>


                                        <div class="span12" style="padding: 1%; margin-left: 0">
                                            <div class="form-group span4">
                                                <label for="valorTotal"> Total: </label>
                                                <input type="text" id="valorTotal" name="valorTotal"
                                                       class="form-control" value="<?php echo $os->valorTotal; ?>">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Continuar</button>
                                        <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>