<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">
    <?php if( $this->session->flashdata('error_msg') ): ?>
        <div class="alert alert-danger" role="alert" ">
            <ul>
                <?php foreach ($this->session->flashdata('error_msg') as $msg): ?>
                    <li><?php echo $msg; ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            Alterar Dados Cadastrais do Cliente
        </div>

        <div class="panel-body">
            <div style="max-width: auto; margin:0">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                        <form method="post" action="<?= base_url('clientes/gravaAlteracao') ?>"
                              enctype="multipart/form-data">

                            <input type="hidden" name="id" value="<?= $clientes->id ?>">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nomeCliente"> Nome: </label>
                                    <input type="text" id="nomeCliente" name="nomeCliente"
                                           value="<?= $clientes->nomeCliente ?>"
                                           class="form-control" required>
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="documento"> CPF/CNPJ: </label>
                                    <input type="text" id="documento" name="documento"
                                           value="<?= $clientes->documento ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="telefone"> Telefone: </label>
                                    <input type="text" id="telefone" name="telefone"
                                           value="<?= $clientes->telefone ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="email"> E-mail </label>
                                    <input type="text" id="email" name="email"
                                           value="<?= $clientes->email ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="celular"> Celular: </label>
                                    <input type="text" id="celular" name="celular"
                                           value="<?= $clientes->celular ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="rua"> Endereço: </label>
                                    <input type="text" id="rua" name="endereco"
                                           value="<?= $clientes->endereco ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="numero"> Numero: </label>
                                    <input type="text" id="numero" name="numero"
                                           value="<?= $clientes->numero ?>"
                                           class="form-control" required>
                                </div>
                            </div>


                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="bairro"> Bairro: </label>
                                    <input type="text" id="bairro" name="bairro"
                                           value="<?= $clientes->bairro ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="cidade"> Cidade: </label>
                                    <input type="text" id="cidade" name="cidade"
                                           value="<?= $clientes->cidade ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label for="estado"> Estado: </label>
                                    <input type="text" id="nome" name="estado"
                                           value="<?= $clientes->estado ?>"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="cep"> CEP: </label>
                                    <input type="text" id="nome" name="cep"
                                           value="<?= $clientes->cep ?>"
                                           class="form-control" required>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-danger"
                                        onclick="window.location.href='/clientes'"> Cancelar </button>
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Gravar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>
