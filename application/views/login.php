<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SystemOS</title>

        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/matrix-style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/matrix-media.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
        <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        <script type="text/javascript"  src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
        <script src="js/jquery.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
<!--        <div class="container-fluid">-->
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="margin-left:30px">SystemOS</a>
            </div>
<!--        </div>-->
    </nav>
    <div class="todo background" style="padding-top: 10%;">
        <div class="row">
            <div class="col-md-offset-4 col-md-4">

                <?php if( $this->session->flashdata('error_msg') ): ?>
                    <div class="alert alert-danger text-center" role="alert">
                        <h5>
                            <b>
                                <?php echo $this->session->flashdata('error_msg') ?>
                            </b>
                        </h5>
                    </div>
                <?php endif ?>

                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <form class="form-signin" id="login" method="post" action="<?= base_url('home/logar') ?>">
                            <h2 class="form-signin-heading" id="titulo" style="padding-bottom: 10px;"> <b>Área Restrita</b> </h2>
                            <label for="inputEmail" class="sr-only">E-mail</label>
                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-mail de acesso" required autofocus><br>
                            <label for="inputPassword" class="sr-only">Senha</label>
                            <input type="password" name="senha" id="inputPassword" class="form-control" placeholder="Senha" required><br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div> 
    </div> 
    </body>
</html>