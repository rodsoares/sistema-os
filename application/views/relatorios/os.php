<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>SystemOS - Email de Resumo de OS</title>
</head>
<body>
    <style>
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        table.minimalistBlack td, table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }
    </style>
    <div style="text-align: center">
        <h1>SystemOS</h1>
        <h4>Relatórios de Ordem de Serviço</h4>
    </div>

    <hr />

    <table class="minimalistBlack">
        <thead>
        <tr>

            <th>OS</th>
            <th>Cliente</th>
            <th>Data </th>
            <th>Defeito</th>
            <th>Status</th>
        </thead>
        <tbody>
        <?php if ( $os ):?>
            <?php foreach ($os as $o) { ?>
                <tr class="text-center">
                    <td> <?= $o->os ?> </td>
                    <td> <?= $o->cliente?> </td>
                    <td> <?= date_format(date_create($o->dataInicial), "d/m/Y") ?> </td>
                    <td> <?= $o->defeito ?> </td>
                    <td>
                        <?php if ( $o->status === 'O'): ?>
                            Orçamento
                        <?php elseif ( $o->status === 'E'): ?>
                            Em andamento
                        <?php elseif ( $o->status === 'A'): ?>
                            Aberto
                        <?php elseif ( $o->status === 'F'): ?>
                            Finalizado
                        <?php elseif ( $o->status === 'C'): ?>
                            Cancelado
                        <?php endif?>
                    </td>
                </tr>
            <?php } ?>
        <?php else: ?>
            <tr class="text-center bg-warning">
                <td colspan="8">
                    <b>SEM RESULTADOS</b>
                </td>
            </tr>
        <?php endif ?>
        </tbody>
    </table>
</body>
