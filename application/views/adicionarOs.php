<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>

<div style="max-width: 90%; margin: 0 auto;">
    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-body">
            <form class="form" method="post" action="<?= base_url('os/gravaos') ?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="clientes_id" >Cliente<span class="required">*</span></label>
                            <select name="clientes_id" id="nomeCliente" class="form-control" required>
                                <option value=""> Selecione... </option>
                                <?php foreach ($clientes as $cliente) { ?>
                                    <option value="<?= $cliente->id ?>"> <?= $cliente->nomeCliente ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="usuario">Técnico/Usuário<span class="required">*</span></label>
                            <select name="usuarios_id" id="nome" class="form-control" required>
                                <option value=""> Selecione... </option>
                                <?php foreach ($usuarios as $usuario) { ?>
                                    <option value="<?= $usuario->id ?>"> <?= $usuario->nome ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="status">Status:<span class="required"></span></label>
                            <select class="form-control" name="status" id="status">
                                <option value="O">Orçamento</option>
                                <option value="A">Aberto</option>
                                <option value="E">Em Andamento</option>
                                <option value="F">Finalizado</option>
                                <option value="C">Cancelado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="dataInicial">Data entrada:<span class="required">:</span></label>
                            <input class="form-control" id="dataInicial" type="text" name="dataInicial" value="<?php echo date('d/m/Y'); ?>"  />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="descricaoProduto">Descrição Equipamento:</label>
                            <textarea class="form-control" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="defeito">Defeito</label>
                            <textarea class="form-control" name="defeito" id="defeito" cols="30" rows="5"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="observacoes">Observações</label>
                            <textarea class="form-control" name="observacoes" id="observacoes" cols="30" rows="5"></textarea>
                        </div>

                        <div class="text-right" >
                            <button onclick="window.location.href='/os'" class="btn btn-danger" type="button"><i class="icon-arrow-left"></i> Voltar</button>
                            <button class="btn btn-success" id="btnGravar" type="submit"><i class="icon-share-alt icon-white"></i> Gravar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    form .row {
        margin-left: 0px;
        margin-right: 0px;
    }
</style>
<?php include 'inc/rodape.php' ?>
