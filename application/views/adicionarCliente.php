<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">
    <?php if( $this->session->flashdata('error_msg') ): ?>
        <div class="alert alert-danger" role="alert" style="margin-top: 10px;">
            <ul>
                <?php foreach ($this->session->flashdata('error_msg') as $msg): ?>
                    <li><?php echo $msg; ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>

    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading">
            <span class="icon">
                <i class="icon-user"></i>
            </span>
            Adicionar Cliente
        </div>
        <div class="panel-body">
            <div style="max-width: auto; margin:0">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                        <form method="post" action="<?= base_url('clientes/gravaCliente') ?>"
                              enctype="multipart/form-data">


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nomeCliente"> Nome: </label>
                                    <input type="text" id="nomeCliente" name="nomeCliente"
                                           class="form-control"
                                           value="<?php echo set_value('nomeCliente') ?>"required>
                                </div>
                            </div>


                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="documento"> CPF/CNPJ: </label>
                                    <input type="text" id="documento" name="documento"
                                           class="form-control cpf"
                                           value="<?php echo set_value('documento') ?>"required>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="telefone"> Telefone: </label>
                                    <input type="text" id="telefone" name="telefone"
                                           class="form-control"
                                           value="<?php echo set_value('telefone') ?>"required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="email"> E-mail </label>
                                    <input type="text" id="email" name="email"
                                           class="form-control"
                                           value="<?php echo set_value('email') ?>"required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="celular"> Celular: </label>
                                    <input type="text" id="celular" name="celular"
                                           class="form-control"
                                           value="<?php echo set_value('celular') ?>"required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="endereco"> Av / Rua: </label>
                                    <input type="text" id="rua" name="endereco"
                                           class="form-control"
                                           value="<?php echo set_value('endereco') ?>" required>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="numero"> Numero: </label>
                                    <input type="text" id="numero" name="numero"
                                           maxlength="5"
                                           class="form-control"
                                           value="<?php echo set_value('numero') ?>" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="cidade"> Cidade: </label>
                                    <input type="text" id="nome" name="cidade"
                                           class="form-control"
                                           value="<?php echo set_value('nome') ?>" required>
                                </div>
                            </div>





                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="bairro"> Bairro: </label>
                                    <input type="text" id="bairro" name="bairro"
                                           class="form-control" required>
                                </div>
                            </div>



                            <div class="col-sm-1">
                                <div class="form-group">
                                    <label for="estado"> Estado: </label>
                                    <input type="text" id="nome" name="estado"
                                           class="form-control"
                                           maxlength="2" required>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="cep"> CEP: </label>
                                    <input type="text" id="nome" name="cep"
                                           class="form-control"
                                           maxlength="9" required>
                                </div>
                            </div>
                            <br/>
                            <div class="text-right">
                                <button type="submit" class="btn btn-danger"
                                        onclick="window.location.href='/clientes'"> Cancelar </button>
                                <button type="submit" class="btn btn-success"><i class="icon-plus icon-white">&nbsp;</i>Cadastrar cliente</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>
<script>
    $(document).ready( function () {
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    });
</script>
<?php include 'inc/rodape.php' ?>