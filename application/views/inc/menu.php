<!--sidebar-menu-->
<div id="sidebar" style="margin-top: -20px"> 
    <ul>
        <li class="<?php
        if (isset($menuPainel)) {
            echo 'active';
        }
        ?>"><a href="<?php echo base_url() ?>"><i class="icon icon-home"></i> <span>Administração</span></a></li>

        <li class="<?php
        if (isset($menuClientes)) {
            echo 'active';
        }
        ?>">
            <a href="<?php echo base_url('clientes') ?>"><i class="icon icon-group"></i> <span>Clientes</span></a></li>


        <li class="<?php
        if (isset($menuClientes)) {
            echo 'active';
        }
        ?>">
            <a href="<?php echo base_url('os') ?>"><i class="icon icon-group"></i> <span>Ordem Serviço</span></a></li>


        <li class="<?php
        if (isset($menuClientes)) {
            echo 'active';
        }
        ?>">
            <a href="<?php echo base_url('fornecedor') ?>"><i class="icon icon-group"></i> <span>Fornecedores</span></a></li>


        <li class="<?php
        if (isset($menuClientes)) {
            echo 'active';
        }
        ?>">
            <a href="<?php echo base_url('usuarios') ?>"><i class="icon icon-group"></i> <span>Usuários</span></a></li>

        


        <li class="submenu <?php if (isset($menuClientes)) {
                echo 'active open';
            }; ?>" >
            <a href="#"><i class="icon icon-list-alt"></i> <span>Relatórios</span> <span class="label"><i class="icon-chevron-down"></i></span></a>
            <ul>
                <li><a href="<?php echo base_url() ?>relatorio/clientes">Clientes</a></li>     
                <li><a href="<?php echo base_url() ?>relatorio/os">O.S</a></li>
                <li><a href="<?php echo base_url() ?>relatorio/osO">O.S - Em orçamento</a></li>
                <li><a href="<?php echo base_url() ?>relatorio/osE">O.S - Em andamento</a></li>
                <li><a href="<?php echo base_url() ?>relatorio/osA">O.S - Ativos</a></li>
                <li><a href="<?php echo base_url() ?>relatorio/osF">O.S - Finalizadas</a></li>
                <li><a href="<?php echo base_url() ?>relatorio/osC">O.S - Canceladas</a></li>
                
            </ul>
        </li>
    </ul>
</div>
<div id="content">
    <div id="content-header" style="margin-top: -20px">
        <div id="breadcrumb">
            <a href="<?php echo base_url() ?>" title="Dashboard" class="tip-bottom">
                <i class="icon-home"></i> Dashboard</a>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/matrix.js"></script> 



