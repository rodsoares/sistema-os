<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>

<div style="max-width: 90%; margin: 0 auto;">
    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading"><i class="glyphicon glyphicon-envelope">&nbsp;</i>Envio de Ordem de Serviço</div>
        <div class="panel-body">
            <form method="post" action="<?= base_url('os/envia') ?>">
                <div class="form-group">
                    <label for="destinatario">Destinatário:</label>
                    <input type="text" class="form-control" id="destinatario"
                           name="destinatario"
                           value="<?php echo $os->nomeCliente ?>">
                </div>
                <div class="form-group">
                    <label for="email">E-mail do Destinatário:</label>
                    <input type="email" class="form-control" id="email"
                           name="email"
                           value="<?php echo $os->clienteEmail ?>">
                </div>
                <div class="form-group">
                    <label for="assunto">Assunto:</label>
                    <input type="text" class="form-control" id="assunto"
                           name="assunto" value="Ordem de Serviço">
                </div>

                <div class="form-group">
                    <label for="mensagem">Mensagem:</label>
                    <textarea class="form-control" id="mensagem" name="mensagem"></textarea>
                </div>

                <button type="submit" class="btn btn-default">Enviar</button>
            </form>
        </div>
    </div>
</div>

<?php include 'inc/rodape.php'?>