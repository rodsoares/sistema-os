<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">
    <div class="panel panel-default" style="margin-top: 10px;">
        <div class="panel-heading">
                <span class="icon">
                    <i class="icon-user">&nbsp;</i>
                 </span>
            Adicionar Usuário
        </div>
        <div class="panel-body">
            <div style="max-width: auto; margin:0">
                <div class="col-lg-12 col-md-12">
                    <div class="row">
                        <form method="post" action="<?= base_url('usuarios/gravausuario') ?>"
                              enctype="multipart/form-data">


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nome"> Nome: </label>
                                    <input type="text" id="nomeCliente" name="nome"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email"> E-mail </label>
                                    <input type="text" id="email" name="email"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="senha"> Senha: </label>
                                    <input type="text" id="documento" name="senha"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="cpf"> CPF: </label>
                                    <input type="text" id="documento" name="cpf"
                                           class="form-control" required>
                                </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="dataCadastro">Data Cadastro:</label>
                                    <input class="form-control" id="dataCadastro" type="text" name="dataCadastro"
                                           value="<?php echo date('d/m/Y'); ?>" />
                                </div>
                            </div>
                            <br/>
                            <div class="form-actions">
                                <div class="span12">
                                    <div class="span6 offset3">
                                        <button type="submit" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'inc/rodape.php' ?>