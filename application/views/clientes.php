<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>


<div style="max-width: 90%; margin: 0 auto;">

    <?php if( $this->session->flashdata('success_msg') ): ?>
        <div class="alert alert-success text-center" role="alert" style="margin-top: 10px;">
            <?php echo $this->session->flashdata('success_msg'); ?>
        </div>
    <?php endif ?>

    <div class="panel panel-default" style="margin-top: 10px;">

        <div class="panel-heading">Clientes > listagem geral</div>

        <div class="panel-body">
            <div style='margin-top: 20px'>
                <a href="clientes/adicionar" class="btn btn-success"><i class="icon-plus icon-white"></i> Adicionar Cliente</a>
            </div>

            <div class="widget-title table-bordered" style='margin-top: 30px'>
                <span class="icon">
                    <i class="icon-user"></i>
                </span>
                <h5>Cliente</h5>
            </div>



            <div class="widget-content nopadding">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>CPF/CNPJ</th>
                            <th>Telefone</th>
                            <th colspan="2">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ( isset($clientes) ): ?>
                            <?php foreach ($clientes as $cli) { ?>
                                <tr class="text-center">
                                    <td> <?= $cli->id ?> </td>
                                    <td> <?= $cli->nomeCliente ?> </td>
                                    <td> <?= $cli->documento ?> </td>
                                    <td> <?= $cli->telefone ?> </td>
                                    <td>
                                        <a href="<?= base_url() . 'clientes/delete/' . $cli->id; ?>"
                                           onclick="return confirm('Confirma Exclusão do Cliente \'<?= $cli->nomeCliente ?>\'?')">
                                            <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                                    </td>

                                    <td>
                                        <a href="<?= base_url() . 'clientes/alterar/' . $cli->id ?>"
                                           onclick="return confirm('Gostaria de alterar os dados do Cliente \'<?= $cli->nomeCliente ?>\'?')">
                                            <span class="glyphicon glyphicon-pencil" title="alterar"></span></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php else:?>
                        <tr class="text-center bg-warning">
                            <td colspan="6">
                                <b>SEM RESULTADOS</b>
                            </td>
                        </tr>
                    <?php endif?>
                    </tbody>
                </table>
            </div>

            <?php echo $pagination; ?>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>






