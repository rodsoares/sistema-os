-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema banco
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema banco
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `banco` DEFAULT CHARACTER SET latin1 ;
USE `banco` ;

-- -----------------------------------------------------
-- Table `banco`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`clientes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCliente` VARCHAR(255) NOT NULL,
  `documento` INT(15) NOT NULL,
  `telefone` INT NOT NULL,
  `celular` INT NULL,
  `email` VARCHAR(50) NOT NULL,
  `dataCadastro` DATE NULL,
  `senha` VARCHAR(6) NULL,
  `cep` VARCHAR(45) NULL,
  `endereco` VARCHAR(45) NULL,
  `cidade` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  `numero` VARCHAR(45) NULL,
  `complemento` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`lancamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`lancamentos` (
  `idLancamentos` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(255) NULL DEFAULT NULL,
  `valor` VARCHAR(15) NOT NULL,
  `data_vencimento` DATE NOT NULL,
  `data_pagamento` DATE NULL DEFAULT NULL,
  `baixado` TINYINT(1) NULL DEFAULT NULL,
  `cliente_fornecedor` VARCHAR(255) NULL DEFAULT NULL,
  `forma_pgto` VARCHAR(100) NULL DEFAULT NULL,
  `tipo` VARCHAR(45) NULL DEFAULT NULL,
  `anexo` VARCHAR(250) NULL,
  `clientes_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idLancamentos`),
  INDEX `fk_lancamentos_clientes1` (`clientes_id` ASC),
  CONSTRAINT `fk_lancamentos_clientes1`
    FOREIGN KEY (`clientes_id`)
    REFERENCES `banco`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`usuarios` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(40) NOT NULL,
  `ativo` INT(1) NOT NULL,
  `dataCadastro` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`os`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`os` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `dataInicial` DATE NULL DEFAULT NULL,
  `dataFinal` DATE NULL DEFAULT NULL,
  `garantia` VARCHAR(20) NULL DEFAULT NULL,
  `descricaoProduto` VARCHAR(150) NULL DEFAULT NULL,
  `defeito` VARCHAR(150) NULL DEFAULT NULL,
  `status` VARCHAR(10) NULL DEFAULT NULL,
  `observacoes` VARCHAR(150) NULL DEFAULT NULL,
  `laudoTecnico` VARCHAR(150) NULL DEFAULT NULL,
  `valorTotal` DECIMAL(10,2) NULL DEFAULT NULL,
  `clientes_id` INT(11) NOT NULL,
  `usuarios_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_os_clientes1` (`clientes_id` ASC),
  INDEX `fk_os_usuarios1_idx` (`usuarios_id` ASC),
  CONSTRAINT `fk_os_clientes1`
    FOREIGN KEY (`clientes_id`)
    REFERENCES `banco`.`clientes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_os_usuarios1`
    FOREIGN KEY (`usuarios_id`)
    REFERENCES `banco`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`produtos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`produtos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(80) NOT NULL,
  `unidade` VARCHAR(2) NULL DEFAULT NULL,
  `precoCompra` DECIMAL(10,2) NULL DEFAULT NULL,
  `precoVenda` DECIMAL(10,2) NOT NULL,
  `estoque` INT(11) NOT NULL,
  `estoqueMinimo` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`produtos_os`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`produtos_os` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quantidade` INT(11) NOT NULL,
  `os_id` INT(11) NOT NULL,
  `produtos_id` INT(11) NOT NULL,
  `preco` DECIMAL(10,2) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_produtos_os_os1` (`os_id` ASC),
  INDEX `fk_produtos_os_produtos1` (`produtos_id` ASC),
  CONSTRAINT `fk_produtos_os_os1`
    FOREIGN KEY (`os_id`)
    REFERENCES `banco`.`os` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_produtos_os_produtos1`
    FOREIGN KEY (`produtos_id`)
    REFERENCES `banco`.`produtos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`servicos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`servicos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NULL DEFAULT NULL,
  `preco` DECIMAL(10,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`servicos_os`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`servicos_os` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `os_id` INT(11) NOT NULL,
  `servicos_id` INT(11) NOT NULL,
  `preco` DECIMAL(10,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_servicos_os_os1` (`os_id` ASC),
  INDEX `fk_servicos_os_servicos1` (`servicos_id` ASC),
  CONSTRAINT `fk_servicos_os_os1`
    FOREIGN KEY (`os_id`)
    REFERENCES `banco`.`os` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_servicos_os_servicos1`
    FOREIGN KEY (`servicos_id`)
    REFERENCES `banco`.`servicos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `banco`.`qualificacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`qualificacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qualidade` INT(2) NULL,
  `data` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `banco`.`fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`fornecedor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `cnpj` INT NULL,
  `email` VARCHAR(45) NULL,
  `telefone` INT NULL,
  `dataCad` VARCHAR(45) NULL,
  `endereco` VARCHAR(45) NULL,
  `cidade` VARCHAR(45) NULL,
  `bairro` VARCHAR(45) NULL,
  `estado` VARCHAR(45) NULL,
  `cep` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `banco`.`interacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`interacao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `obs_loja` VARCHAR(150) NULL,
  `obs_cliente` VARCHAR(150) NULL,
  `date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `banco`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`compras` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nf` VARCHAR(10) NULL,
  `data` DATE NULL,
  `fornecedor_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_compras_fornecedor1_idx` (`fornecedor_id` ASC),
  CONSTRAINT `fk_compras_fornecedor1`
    FOREIGN KEY (`fornecedor_id`)
    REFERENCES `banco`.`fornecedor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `banco`.`itensCompras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `banco`.`itensCompras` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `produtos_id` INT(11) NOT NULL,
  `compras_id` INT NOT NULL,
  `quantidade` INT(10) NULL,
  `valor` FLOAT(10,2) NULL,
  INDEX `fk_itensCompras_produtos1_idx` (`produtos_id` ASC),
  INDEX `fk_itensCompras_compras1_idx` (`compras_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_itensCompras_produtos1`
    FOREIGN KEY (`produtos_id`)
    REFERENCES `banco`.`produtos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_itensCompras_compras1`
    FOREIGN KEY (`compras_id`)
    REFERENCES `banco`.`compras` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
