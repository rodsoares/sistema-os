<?php

class Os extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->gerenciar();
    }

    function gerenciar() {
        $this->load->view('consultaOs');
    }
}
