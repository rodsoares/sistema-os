<?php include 'inc/cabecalho.php' ?>
<?php include 'inc/menu.php' ?>
<div class="container" style="margin-left: 3%">
    <div class="row-fluid" style="margin-top:0; max-width: 90%">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-tags"></i>
                    </span>
                    <h5>Consulta Ordem Serviço</h5>
                </div>
                <div class="widget-content nopadding">
                    <div class="span12" id="consultarOS" style=" margin-left: 0">
                        <ul class="nav nav-tabs">
                            <li class="active" id="tabDetalhes"><a href="#tab1" data-toggle="tab">Detalhes da OS</a></li>
                        </ul>




                        <?php
                        // se o campo id não estiver definido (ou seja, o form não submetido)
                        if (isset($_POST["id"])) {
                            // obtém o id passado pelo form
                            $id = htmlspecialchars($_POST["id"]);
                            $url = "http://localhost/ordemservico/Webservice/os/" . $id;
                            // incializa a biblioteca curl que permite que uma página seja carregada
                            $pagina = curl_init($url);
                            // define as configurações da chamada (basicamente que o retorno seja transferido
                            // para uma variável)
                            curl_setopt($pagina, CURLOPT_RETURNTRANSFER, 1);
                            // atribui para a variável o conteúdo retornado pela chamada curl à url
                            $reposta = curl_exec($pagina);
                            // converte a resposta json para um objeto
                            $json = json_decode($reposta);
                            if ($json->situacao == "encontrado") {
                                
                            } else {
                                echo "<h3> Erro! OS não cadastrada </h3>";
                            }
                        }
                        ?> 

                        <div class="tab-content" style="padding: 1%">
                            <div class="tab-pane active" id="tab1">
                                <div class="span12" id="divConsultarOS">
                                    <div class="span2" style="padding: 1%">
                                        <form method="post">
                                            <div class="form-group">
                                                <label for="id">Ordem Serviço: </label>
                                                <input type="text" id="id" name="id"
                                                       class="form-control" placeholder="Número da OS" required>
                                            </div>
                                            <div class="span2" style="padding: 1%">
                                                <button type="submit" class="btn btn-success" value="Pesquisar">Pesquisar</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="span12" style="padding: 1%; margin-left: 0.1em">
                                        <div class="span6">
                                            <label for="cliente">Cliente:<span class="required"></span></label>
                                            <input id="cliente" class="form-control" type="text" name="cliente" disabled value="<?php if (isset($json)) echo $json->cliente; ?>">
                                        </div>

                                        <div class="span6">
                                            <label for="tecnico">Técnico:<span class="required"></span></label>
                                            <input id="status" class="form-control" type="text" name="status" disabled value="<?php if (isset($json)) echo $json->tecnico; ?>">
                                        </div>
                                    </div>


                                    <div class="span12" style="padding: 1%; margin-left: 0.1em">
                                        <div class="span4">
                                            <label for="status">Status:</label>
                                            <input id="status" class="form-control" type="text" name="status" disabled value="<?php if (isset($json)) echo $json->status; ?>">
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="descricaoProduto">Descrição Equipamento:</label>
                                            <textarea class="span12" name="descricaoProduto" id="descricaoProduto" cols="30" rows="5" disabled><?php if (isset($json)) echo $json->descricao; ?></textarea>
                                        </div>
                                        <div class="span6">
                                            <label for="defeito">Defeito:</label>
                                            <textarea class="span12" name="defeito" id="defeito" cols="30" rows="5" disabled><?php if (isset($json)) echo $json->defeito; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">
                                        <div class="span6">
                                            <label for="laudo">Laudo:</label>
                                            <textarea class="span12" name="laudo" id="defeito" cols="30" rows="5" disabled><?php if (isset($json)) echo $json->laudotecnico; ?></textarea>
                                        </div>

                                        <div class="span6">
                                            <label for="areaCliente">Área do Cliente:</label>
                                            <textarea class="span12" name="areaCliente" id="areacliente" cols="30" rows="5"></textarea>
                                        </div>

                                    </div>

                                    <div class="span12" style="padding: 1%; margin-left: 0">

                                        <div class="form-group span4">
                                            <label for="total"> Valor Peças: </label>
                                            <input type="text" id="total" name="valorpecas"
                                                   class="form-control" disabled value="<?php if (isset($json)) echo $json->valorpecas; ?>">
                                        </div>

                                        <div class="form-group span4">
                                            <label for="valorServico"> Valor Serviço: </label>
                                            <input type="text" id="valorServico" name="valorServico"
                                                   class="form-control" disabled value="<?php if (isset($json)) echo $json->valorservico; ?>">
                                        </div>

                                        <div class="form-group span4">
                                            <label for="total"> Total: </label>
                                            <input type="text" id="total" name="total"
                                                   class="form-control" disabled value="<?php if (isset($json)) echo $json->valortotal; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="span12" style="padding: 1%; margin-left: 0">
                                    <div class="span6 offset3" style="text-align: center">
                                        <button class="btn btn-success" id="btnContinuar"><i class="icon-share-alt icon-white"></i> Enviar</button>
                                        <a href="<?php echo base_url() ?>index.php/os" class="btn"><i class="icon-arrow-left"></i> Voltar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'inc/rodape.php' ?>
